package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {


	@Test
	public void testIsPalindrome( ) {
		boolean test = Palindrome.isPalindrome("racecar");
		assertTrue("This is pelindrom string", test );

	}

	@Test
	public void testIsPalindromeException( ) {
		assertFalse("This is not a pelindrom string", Palindrome.isPalindrome("vishwa"));
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		assertTrue("This is pelindrom string", Palindrome.isPalindrome("Taco cat"));
		
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		assertFalse("This is pelindrom string", Palindrome.isPalindrome("race on car "));
	}
}
